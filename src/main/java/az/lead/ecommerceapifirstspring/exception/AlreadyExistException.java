package az.lead.ecommerceapifirstspring.exception;

import az.lead.ecommerceapifirstspring.enums.ResultCode;

public class AlreadyExistException extends BaseException{


    public AlreadyExistException(ResultCode result) {
        super(result);
    }
    public static AlreadyExistException productAlreadyExist(){
        return new AlreadyExistException(ResultCode.PRODUCT_ALREADY_EXIST);
    }
    public static AlreadyExistException productCodeAlreadyExist(){
        return new AlreadyExistException(ResultCode.PRODUCT_CODE_ALREADY_EXIST);
    }
}
