package az.lead.ecommerceapifirstspring.controller;

import az.lead.ecommerceapifirstspring.dto.ProductDto;
import az.lead.ecommerceapifirstspring.model.ListResult;
import az.lead.ecommerceapifirstspring.model.SingleResult;
import az.lead.ecommerceapifirstspring.request.ProductReq;
import az.lead.ecommerceapifirstspring.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("products")
@RequiredArgsConstructor
@Slf4j
public class ProductController {

    private final ProductService productService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @GetMapping()
    public ListResult<ProductDto> findAll(){
        System.out.println(bCryptPasswordEncoder.encode("12345"));
        log.info("endpoint called: /products , method: GET");
        return new ListResult<>(productService.findAll());
    }

    @GetMapping("/{id}")
    public SingleResult<ProductDto>  findById(@PathVariable("id") Long id){
        log.info("endpoint called: /products/{} , method: GET",id);
        return new SingleResult<>(productService.findById(id));
    }

    @PutMapping("/{id}")
    public SingleResult<ProductDto> update(@PathVariable("id") Long id,@Valid @RequestBody ProductReq productReq){
        log.info("endpoint called: /products/{} {} , method: PUT",id,productReq);
        return new SingleResult<>(productService.update(id,productReq));
    }


    @PostMapping
    public SingleResult<ProductDto> save(@Valid @RequestBody ProductReq productReq){
        log.info("endpoint called: /products/{} , method: GET",productReq);
        return new SingleResult<>(productService.save(productReq));
    }

}
