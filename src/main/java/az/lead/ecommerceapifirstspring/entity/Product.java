package az.lead.ecommerceapifirstspring.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product")
public class Product extends CommonAuditEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_type_id")
    private ProductType productTypeId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_id")
    private Unit unitId;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Double price;

    @Column(name = "is_active",insertable = false)
    private Integer isActive;

    @Column(name = "product_code")
    private String productCode;

    public Product(ProductType productTypeId, Unit unitId, String name, Double price) {
        this.productTypeId = productTypeId;
        this.unitId = unitId;
        this.name = name;
        this.price = price;
    }
}