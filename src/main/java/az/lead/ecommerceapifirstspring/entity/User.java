package az.lead.ecommerceapifirstspring.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_login_id")
    private UserLogin userLoginId;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;
}
