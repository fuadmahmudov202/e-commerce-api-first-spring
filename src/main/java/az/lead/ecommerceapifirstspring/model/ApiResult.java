package az.lead.ecommerceapifirstspring.model;

import az.lead.ecommerceapifirstspring.constants.AppConstants;
import az.lead.ecommerceapifirstspring.enums.ResultCode;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;
@Data
/*@RequiredArgsConstructor*/
public class ApiResult {
    private final int code;
    private final String message;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConstants.DATE_TIME_FORMAT)
    private LocalDateTime timeStamp;

    public ApiResult(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getValue();
        this.timeStamp = LocalDateTime.now();
    }
    public ApiResult(int code,String message) {
        this.code = code;
        this.message = message;
        this.timeStamp = LocalDateTime.now();
    }
}
