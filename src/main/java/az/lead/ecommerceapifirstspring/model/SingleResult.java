package az.lead.ecommerceapifirstspring.model;

import az.lead.ecommerceapifirstspring.enums.ResultCode;
import lombok.Getter;

@Getter
public class SingleResult<M> extends ApiResult{
    private M data;


    public SingleResult(ResultCode resultCode, M data) {
        super(resultCode);
        this.data = data;
    }
    public SingleResult(M data){
        super(ResultCode.OK);
        this.data=data;
    }
}
