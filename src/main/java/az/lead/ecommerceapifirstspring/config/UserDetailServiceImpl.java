package az.lead.ecommerceapifirstspring.config;

import az.lead.ecommerceapifirstspring.exception.DataNotFoundException;
import az.lead.ecommerceapifirstspring.repository.UserLoginRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Primary
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserLoginRepo userLoginRepo;

    @Override
    public UserDetails loadUserByUsername(String username) {
        var userLogin=userLoginRepo.findByUsername(username)
                .orElseThrow(DataNotFoundException::userNotFound);
        return new UserDetailsImpl(userLogin);
    }
}
