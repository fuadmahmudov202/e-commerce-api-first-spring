package az.lead.ecommerceapifirstspring.mapper;

import az.lead.ecommerceapifirstspring.dto.ProductDto;
import az.lead.ecommerceapifirstspring.entity.Product;
import az.lead.ecommerceapifirstspring.entity.ProductType;
import az.lead.ecommerceapifirstspring.entity.Unit;
import az.lead.ecommerceapifirstspring.enums.Status;
import az.lead.ecommerceapifirstspring.request.ProductReq;

import java.util.Objects;

public class ProductMapper {
//    public static Product mapToEntity(ProductReq productReq, ProductType productType, Unit unit) {
//        Product product = new Product();
//        product.setName(productReq.getName());
//        product.setProductCode(productReq.getCode());
//        product.setProductTypeId(productType);
//        product.setUnitId(unit);
//        product.setPrice(productReq.getPrice());
//        return product;
//    }
//    public static Product mapToEntityForUpdate(ProductReq productReq, ProductType productType, Unit unit, Long id) {
//        Product product = new Product();
//        product.setId(id);
//        product.setName(productReq.getName());
//        product.setProductCode(productReq.getCode());
//        product.setProductTypeId(productType);
//        product.setUnitId(unit);
//        product.setPrice(productReq.getPrice());
//        product.setIsActive(Status.ENABLED.getValue());
//        return product;
//    }
//
//    public static ProductDto mapToDto(Product product) {
//
//        ProductDto productDto = new ProductDto();
//        ProductType productType = product.getProductTypeId();
//        Unit unit =product.getUnitId();
//        if (Objects.nonNull(productType)&&Objects.nonNull(unit)) {
//            productDto.setProductTypeId(productType.getId());
//            productDto.setUnitId(unit.getId());
//        }
//        productDto.setId(product.getId());
//        productDto.setName(product.getName());
//        productDto.setPrice(product.getPrice());
//        productDto.setIsActive(product.getIsActive());
//        productDto.setProductCode(product.getProductCode());
//            return productDto;
//    }
}
