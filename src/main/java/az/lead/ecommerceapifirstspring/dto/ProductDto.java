package az.lead.ecommerceapifirstspring.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
    private Long id;
    private String productTypeId;
    private String unitId;
    private String name;
    private Double price;
    private Integer isActive;
    private String productCode;


}
